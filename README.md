## Curso de JAVA SE 8

[Diapositivas del curso](https://docs.google.com/presentation/d/1W6mJXmbTxMa5FiqU4EEXomGGvanYw-qFoQ1O32q1Pdo/edit?usp=sharing)

Trainer: Luis Roberto <luis.robertop87@gmail.com>

### Software a instalar 
1. [Java Development Kit 8 (JDK)](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
2. [IntelliJ IDEA](https://www.jetbrains.com/idea/). Community Version

* El código fuente del proyecto esta basado en un proyecto Java [Maven](https://maven.apache.org/).

### Instrucciones para abrir el proyecto:

#### Desde una copia local:
1. Abrir IDEA
2. Ir a File -> Open...
3. Buscar la caperta del proyecto **javaeight**
4. Seleccionar el archivo **pom.xml**
5. Presionar OK

#### Desde Repositorio:
1. Ejecutar IntelliJ
2. En la pantalla inicial seleccionar la opción ***Check out from Version Control*** -> ***Git***
3. En URL pegar la dirección *https://bitbucket.org/sb-ccat/java8-course.git*
4. Establecer un folder donde se guardará el proyecto
5. Asignar un nombre el proyecto (ejemplo: java8-course)
6. Presionar el botón **Clone** y aceptar la creación del proyecto.
7. Aceptar la opción de abrir el archivo **pom.xml**

#### Ejemplo de como importar el proyecto en IDEA
- [IntelliJ IDEA](https://youtu.be/iDzy2yZit94) Aplica solo hasta la importación del pom.xml

#### Pasos opcionales
Una vez abierto el proyecto habilitar la auto importación de Maven:
Una notificación será visible en el lado inferior derecho y debe seleccionar `"Enable AutoImport"`

### Contenido del Curso
1. Expresiones Lambda
2. Stream API
3. Programación funcional
4. REST Web Server (Dropwizard)
  - [Repositorio de ejemplo](https://bitbucket.org/ccat_dropwizard/nivel-basico.git)
## Libro base
Java 8 para realmente impacientes. Cay Horstmann
http://horstmann.com/java8/

## REST Web Server
Dentro del proyecto, la carpeta SampleServer continene un proyecto Web independiente

