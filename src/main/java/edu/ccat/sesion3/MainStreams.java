package edu.ccat.sesion3;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainStreams {

  public static void main(String[] args) {
    List<Point> points = Arrays.asList(
        new Point(5, 2)
        , new Point(2, 2)
        , new Point(1, 2)
        , new Point(-5, 1)
        , new Point(6, 4)
        , new Point(7, 6)
        , new Point(7, 6)
    );

    System.out.println("==== Foreach simple ===");
    //points.forEach(System.out::println);
    //points.forEach(p -> Helper.printPoint(p));
    points.forEach(Helper::printPoint);

    // Calcular las suma de todas las coordenadas X
    // int sum = 0;
    // for (Point p : points) sum += p.x;
    int sum = points.stream()
        .mapToInt(p -> p.x)
        //.peek(x -> System.out.println(x))
        .sum();

    // Calcular las suma de todas las coordenadas X con valor base
    int sumReduce = points.stream()
        .mapToInt(p -> p.x)
        .reduce(0, (x1, x2) -> x1 + x2);

    // Calcular las suma de todas las coordenadas X con valor opcional
    points.stream()
        .mapToInt(p -> p.x)
        .reduce((x1, x2) -> x1 + x2)
        .ifPresent(s -> System.out.println("Presente "+ s));

    List<Point> empty = new ArrayList<>(0);
    empty.stream()
        .mapToInt(p -> p.x)
        .reduce((x1, x2) -> x1 + x2)// .orElseThrow(NullPointerException::new)
        .ifPresent(s -> System.out.println("Presente "+ s));


    // Calcular las suma de todas las coordenadas X mayores a 3
    points.stream()
        .filter(p -> p.x > 3)
        .mapToInt(p -> p.x)
        .peek(n -> System.out.println(n))
        .sum();

    // Calcular las suma de todas las coordenadas X diferentes mayores a 3
    System.out.println("==== Distinct ===");
    points.stream()
        .filter(p -> p.x > 3)
        //.distinct() aplicado al objeto point
        .mapToInt(p -> p.x)
        .distinct() // aplicado al valor X entero
        .peek(n -> System.out.println(n))
        .sum();

    // Agregar un punto a la lista original en caso de que el punto sea = 2
    try {
      points.stream()
          .filter(p -> p.x == 2)
          .map(p -> new Point(10 * p.x, p.y)) // lista o valor?
          .forEach(points::add);
    } catch (Exception e) {
      System.out.println(e.getStackTrace().toString());
    }

    // Concatenar streams
    // 1- lista original
    // 2- lista con nuevos valores
    // concatenar 1 y 2
    System.out.println("==== Concatenar streams ===");
    List<Point> newList = Stream.concat(
        points.stream(), // 1
        points.stream()
            .filter(p -> p.x == 2)
            .map(p -> new Point(10*p.x, p.y))  // 2
    //).forEach(Helper::printPoint);
    //).collect(Collectors.toList());
    ).collect(Collectors.toCollection(LinkedList::new));

    System.out.println("==== Recolectar en un solo String ===");
    String output = points.stream()
        .mapToInt(p -> p.x)
        .mapToObj(Integer::toString)
        .collect(Collectors.joining(", "));

    System.out.println(output);

    // getClient(id) -> Client | null
    /*Client aux = getClient(id);
    if (aux != null) {

    }

    // getClient(id) -> Optional<Client>
    //Optional clientAux = Optional.empty();//
    */
  }
}
