package edu.ccat.sesion3;

public class ExecutableImpl implements Executable {
  @Override
  public void exec() {
    System.out.println("My exec implementation");
  }

  @Override
  public void hello() {
    System.out.println("My own Hello");
  }
}
