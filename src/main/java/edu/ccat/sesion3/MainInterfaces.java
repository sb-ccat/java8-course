package edu.ccat.sesion3;

import java.util.Arrays;
import java.util.List;

public class MainInterfaces {

  public static void main(String[] args) {
    ExecutableImpl executable = new ExecutableImpl();

    executable.exec();
    executable.hello();

    ExecutableImpl2 executable2 = new ExecutableImpl2();

    executable2.exec();
    executable2.test();

    executable2.hello(); // ???

    Executable.staticHello();

    List<Integer> list = Arrays.asList(7, 8, 9, 15);

    list.forEach(number -> {
      System.out.println(number);
      Executable.staticHello();
    });
  }
}
