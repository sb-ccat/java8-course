package edu.ccat.sesion3;

@FunctionalInterface
public interface Adder {

  void run();
}
