package edu.ccat.sesion3;

@FunctionalInterface
public interface TestInterface {

  void test();

  default void hello() {
    System.out.println("Hello From Test");
  }
}
