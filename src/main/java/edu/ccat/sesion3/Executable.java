package edu.ccat.sesion3;

@FunctionalInterface
public interface Executable {

  void exec();

  default void hello() {
    System.out.println("Hello from Executable");
  }

  static void staticHello() {
    System.out.println("Hello from Executable");
  }


}
