package edu.ccat.sesion3;

import java.awt.Point;

public class Helper {

  public static void printPoint(Point point) {
    System.out.print(String.format("[ %d , %d ]", point.x, point.y));
  }
}
