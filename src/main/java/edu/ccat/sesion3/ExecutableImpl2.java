package edu.ccat.sesion3;

public class ExecutableImpl2 implements Executable, TestInterface {
  @Override
  public void exec() {
    //
  }

  @Override
  public void test() {

  }

  @Override
  public void hello() {
    Executable.super.hello();
    TestInterface.super.hello();
    System.out.println("My fixed hello");
  }
}
