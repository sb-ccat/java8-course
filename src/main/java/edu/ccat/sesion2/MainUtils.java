package edu.ccat.sesion2;

import edu.ccat.sesion1.Student;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MainUtils {

  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(5, 85, 96, 78, 17, 35, 101, 52);

//    Util.esPar(5);
    System.out.println(sum(numbers, Util::esPar));
    System.out.println(sum(numbers, Util::esImpar));

    // Pendiente con BiFunctions
    List<String> names = Arrays.asList("Roberto", "Juan", "Pablo", "Victor", "Ana");
    System.out.println(
        names.stream()
            .map(x -> x.length())
            .collect(Collectors.toList()));

    List<Student> students = Arrays.asList(
        new Student(),
        new Student("Juan")
    );

    students.stream()
        .mapToInt(s -> s.getAge())
        .average();
  }

  public static int sum(List<Integer> numbers, Predicate<Integer> selector) {
    int result = 0;
    /*for (int number: numbers) {
      if (selector.test(number)) result += number;
    }*/

    return numbers.stream()
        .filter(selector::test)
        //.mapToInt(z -> z)
        //.sum();
        .reduce(0, (x, y) -> x+y);
  }
}
