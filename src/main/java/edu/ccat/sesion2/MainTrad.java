package edu.ccat.sesion2;

import edu.ccat.sesion1.Student;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainTrad {






  public static int sumaTotal(List<Integer> lista)
  {
    int val = 0;
    for (int i=0;i<lista.size(); i++){
      val = val + lista.get(i);
    }
    return val;
  }

  public static int sumaPares(List<Integer> lista)
  {
    int val = 0;
    for (int i=0;i<lista.size(); i++){
      if (lista.get(i)%2 == 0) {
        val = val + lista.get(i);
      }
    }
        return val;
  }
  public static void main(String[] args) {
    // Se cuenta con una lista de enteros y sobre esta lista se debe
    // realizar varias operaciones

    /*List<Student> students = Arrays
        .asList(new Student(),
            new Student("Juan"));*/

    List<Integer> numbers = Arrays.asList(5, 8, 9, 15, 17, 22, 30);
    System.out.println("pares:" + sumaPares(numbers));
    System.out.println("tot: " + sumaTotal(numbers));



  }
}
