package edu.ccat.sesion2;

public class Util {

  public static boolean esPar(int number) {
    return number % 2 == 0;
  }

  public static boolean esPar(int number, int second) {
    return number % 2 == 0;
  }

  public static boolean esImpar(int number) {
    return number % 2 != 0;
  }
}
