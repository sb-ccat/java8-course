package edu.ccat.sesion2;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class MainPredicate {

  public static void main(String[] args) {
    List<Integer> numbers = Arrays.asList(5, 85, 96, 78, 17, 35, 101, 52);

    Predicate<Integer> all = n -> true;
    Predicate<Integer> even = n -> n % 2 != 0;
    Predicate<Integer> odd = n -> n % 2 == 0;
    Predicate<Integer> greaterThan90 = n -> n > 90;

    //all.and(even).or(odd);

    System.out.println(sum(numbers, all));
    System.out.println(sum(numbers, n -> n % 2 == 0));
    System.out.println(sum(numbers, n -> n % 2 != 0));

    //Alternativa via Streams
    System.out.println(
      numbers.stream()
          .filter(odd)
          //.reduce(0, (a, b) -> a+b));
          .reduce(0, Integer::sum));

    // Recolectar la lista filtrada, se debe hacer antes del mapeo
    System.out.println(
        numbers.stream()
          .filter(even)
          .collect(Collectors.toList())
    );

    System.out.println(numbers);
    // Se reduce la lista de pares a las suma duplicada
    System.out.println(
        numbers.stream()
            .filter(even)
            .mapToDouble(e -> 2*e) // get(int e) { return 2*e; }
            .sum()
    );

    System.out.println(numbers);

    // Suma de pares mayores a 30
    System.out.println(sum(numbers, odd.and(greaterThan90)));
    System.out.println(sum(numbers, odd.and(number -> number > 100)));
    System.out.println(sum(numbers, odd.and(number -> number > 100).negate()));
  }

  // Normal Method

  // High-order method
  public static int sum(List<Integer> numbers, Predicate<Integer> selector) {
    int result = 0;
    for (int number: numbers) {
      if (selector.test(number)) result += number;
    }
    return result;
  }
}
