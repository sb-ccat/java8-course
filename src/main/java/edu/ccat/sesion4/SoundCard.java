package edu.ccat.sesion4;

import java.util.Optional;

public class SoundCard {


  private String model;
  private Optional<Usb> usb;

  public SoundCard(String model, Usb usb) {
    this.model = model;
    this.usb = Optional.ofNullable(usb);
  }

  public String getModel() {
    return model;
  }

  public Optional<Usb> getUsb() {
    return usb;
  }
}
