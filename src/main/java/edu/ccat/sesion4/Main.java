package edu.ccat.sesion4;

import java.util.Optional;

public class Main {

  public static void main(String[] args) {
    Computer computer = new Computer("IBM");

    System.out.println(computer.getName());

    System.out.println(computer.getSoundCard());

    //System.out.println(computer.getSoundCard().getModel());
    /*
    if (computer.getSoundCard() != null) {
      System.out.println(computer.getSoundCard().get().getModel());
      if (computer.getSoundCard().get().getUsb() != null)
      System.out.println(computer.getSoundCard().get().getUsb());
    } else {
      // System.out.println(computer.getSoundCard().getUsb());
    }*/

    // En caso de que no tenga acceso a la clase Computer, debo encerrar en un contendor
    // Optional<SoundCard> soundCard = Optional.ofNullable(computer.getSoundCard());

    Computer computer1 = new Computer("IBM", new SoundCard("3.0", new Usb("USB")));

    // Obtener el SoundCard de la computadora si es que la version es 3.0
    //Optional<SoundCard> soundCard =
        computer1.getSoundCard().filter(sc -> "3.0".equals(sc.getModel()))
            .ifPresent(x -> System.out.println("SoundCard encontrado"));

    SoundCard soundCard =
        computer1.getSoundCard().filter(sc -> "2.0".equals(sc.getModel()))
            .orElseGet(() -> new SoundCard("Default", null));

    System.out.println(soundCard);


    System.out.println("Mapero normal sobre Optional");
    computer1.getSoundCard()
//        .filter(o -> o.getUsb().isPresent())
        .map(x -> x.getUsb()) // Mapeo sobre Optional
        //.map(y -> y.) Solo se pudo obtener los valores como Optional
        .ifPresent(System.out::println);

    System.out.println("Mapeo con Flat normal sobre Optional");
    computer1.getSoundCard()
        .flatMap(x -> x.getUsb()) // Mapeo sobre Optional
        //.filter(s -> s.getName().equals(""))
        .map(y -> y.getName()) // Se puede acceder al valor del Optional
        .ifPresent(System.out::println);
  }
}
