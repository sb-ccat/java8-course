package edu.ccat.sesion4;

import org.junit.Test;

import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.junit.Assert.*;

public class MainTest {

  @Test
  public void simpleTest() {
    assertTrue(true);
  }

  @Test
  public void emptyOptional() {
    /*String name = "";
    name = null;*/
    Optional empty = Optional.empty();
    System.out.println(empty);

    // if (obj == null)
    assertFalse(empty.isPresent());
  }

  @Test
  public void nullableValue() {
    Optional<String> name = Optional.of("Roberto");
    assertTrue(name.isPresent());
  }

  @Test
  public void nullableValueNull() {
    Optional<String> name = Optional.of(null);
    assertFalse(name.isPresent());
  }

  @Test
  public void nullableValueNullable() {
    //NoSuchOperationError
    Optional<String> name = Optional.ofNullable(null);
    assertFalse(name.isPresent());
  }

  @Test
  public void getNullUserFromNullable() {
    String noExist = null; // BD devuelve un valor inexistente

    /*
    if (noExist == null) {
      System.out.println("El usuario no existe");
    } else {
      System.out.println("Usuario " + noExist);
    }*/

    Optional<String> user = Optional.ofNullable(noExist);
    user.ifPresent(u -> System.out.println("Usuario " + u));
  }

  @Test
  public void getNonNullUserFromNullable() {
    String exist = "Roberto";
    Optional<String> user = Optional.ofNullable(exist);
    user.ifPresent(u -> System.out.println("Usuario " + u)); // Ejecuta un algoritmo cuando se que el valor existe
  }

  @Test
  public void showDefaultValue() {
    String noExist = null;
    Optional<String> user = Optional.ofNullable(noExist);

    String value = user.orElseGet(() -> { // Ejecuta un algoritmo cuando se que el valor es nullo o no existe
      System.out.println("Abrir una ventana");
      return "Nuevo Usuario";
    });

    System.out.println(value);
  }

  @Test
  public void showDefaultValueUser() {
    Optional<String> user = Optional.ofNullable(null);

    /*if (name == null) {
      return "Anonimo";
    } else {
      return name;
    }*/

    String value = user.orElse("Anonimo");
  }

  @Test
  public void emptyPassword() throws Exception {
    Optional<String> password = Optional.ofNullable(null);

    String pwd = password.orElseThrow(TestExceptions::emptyPasswordException);
  }


  @Test
  public void emptyPasswordExceptionWithMessage() throws Exception {
    Optional<String> password = Optional.ofNullable(null);

    String pwd = password
        .orElseThrow(() -> TestExceptions.customPasswordException("El password no existe o es incorrecto"));
  }

  // Dada una lista se desea evaluar si es que una busqueda en su longitud existe
  @Test
  public void evaluarListas() {
    List<String> names = Arrays.asList("Juan", "Pedro", "Andres");

    // Si recibimos una lista
    // if (list.size > 0) no esta vacia

    // Si recibimos una lista nula
    //if (list != null && list > 0)

    Optional<List<String>> listOptional = Optional.of(names);

    int size = listOptional
        .map(List::size)
        .orElse(0);

    listOptional
        .map(List::size)
        .ifPresent(System.out::println);
  }

  @Test
  public void stringValidations() {
    // Validar longitud de password o que no este vacio
    String key = "clave123";
    Optional<String> keyOptional = Optional.of(key);

    int len = keyOptional
        .map(String::length)
        .orElse(0);
  }

  @Test
  public void encadenamientoFunciones() {
    Function<Integer, String> intToString = x -> x.toString();
    Function<String, Integer> countChars = x -> x.length();
    Function<Integer, Point> intToPoint = x -> new Point(x, x);
    Function<Point, Double> distanceToOrigin = p -> p.distance(0, 0);

    Point point = intToString.andThen(countChars.andThen(intToPoint)).apply(5);
    Point point2 = intToString.andThen(countChars).andThen(intToPoint).apply(5);
    System.out.println(intToString.andThen(countChars.andThen(intToPoint.andThen(distanceToOrigin))).apply(10));
  }

}