package edu.ccat.sesion4;

public class TestExceptions {


  public static Exception emptyPasswordException() {
    return new Exception("El Password esta Vacio");
  }

  public static Exception customPasswordException(String message) {
    return new Exception(message);
  }
}
