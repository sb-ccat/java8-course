package edu.ccat.sesion4;

import java.util.Optional;

public class Computer {


  private String name;
  private Optional<SoundCard> soundCard;

  public Computer(String name) {
    this.name = name;
    this.soundCard = Optional.empty();
  }

  public Computer(String name, SoundCard soundCard) {
    this.name = name;
    this.soundCard = Optional.ofNullable(soundCard);
  }

  public String getName() {
    return name;
  }

  public Optional<SoundCard> getSoundCard() {
    return this.soundCard;
  }

  public SoundCard getDefaultSoundCard() {
    return this.soundCard.orElse(new SoundCard("Generic", null));
  }
}
