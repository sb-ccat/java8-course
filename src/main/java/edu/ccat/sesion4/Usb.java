package edu.ccat.sesion4;

public class Usb {

  private String name;

  public Usb(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
