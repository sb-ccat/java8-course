package edu.ccat.sesion5;

public class Person {

  private String name;
  private long ci;
  private int age;

  public Person(String name, long ci, int age) {
    this.name = name;
    this.ci = ci;
    this.age = age;
  }

  public String getName() {
    return name;
  }

  public long getCi() {
    return ci;
  }

  public int getAge() {
    return age;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("Person{");
    sb.append("name='").append(name).append('\'');
    sb.append(", ci=").append(ci);
    sb.append(", age=").append(age);
    sb.append('}');
    return sb.toString();
  }
}
