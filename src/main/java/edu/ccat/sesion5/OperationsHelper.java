package edu.ccat.sesion5;

import java.awt.Point;

public class OperationsHelper {

  public static int getLength(String s) {
    return s.length();
  }

  public static long sumCoords(Point point) {
    return point.x + point.y;
  }
}
