package edu.ccat.sesion5;

import org.junit.Test;

import javax.swing.text.html.Option;
import java.awt.Point;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MainFunctionalTest {

  // High-order function Una entrada y una salida
  @Test
  public void functionInputOutput() {
    Function<String, Integer> getLength = s -> s.length();
    /*
    * public getLength(String s) {
    *   return s.length();
    * }
    * */

    int length = getLength.apply("Hello");

    assertEquals(length, 5);
  }

  @Test
  public void functionInputOutputOperationHelper() {
    Function<String, Integer> getLength = OperationsHelper::getLength;
    int length = getLength.apply("Hello");
    assertEquals(length, 5);

    // Anonimo
    Optional<String> maybe = Optional.of("HELLO");
    int result = maybe.map(OperationsHelper::getLength).orElse(0);
    assertEquals(result, 5);
  }

  @Test
  public void sumarCoordenadas() {
    Function<Point, Long> sumCoords = p -> (long) (p.x + p.y);

    long result = sumCoords.apply(new Point());
    assertEquals(result, 0); // Caso simple

    List<Point> points = Arrays.asList(
        new Point(),
        new Point(15,26),
        new Point(5,8)
    );

    System.out.println("Lista Original");
    points.forEach(System.out::println);

    System.out.println("Lista Procesada");
    points.stream()
        .map(sumCoords)
        //.map(p -> sumCoords.apply(p))
        .collect(Collectors.toList())
        .forEach(System.out::println);

    System.out.println("Valor reducido");
    System.out.println(points.stream()
        .map(sumCoords)
        .mapToLong(s -> s)
        .sum()
    );
  }

  @Test
  public void sumarCoordenadasOperationHelper() {
    Function<Point, Long> sumCoords = OperationsHelper::sumCoords;

    long result = sumCoords.apply(new Point());
    assertEquals(result, 0); // Caso simple

    List<Point> points = Arrays.asList(
        new Point(),
        new Point(15,26),
        new Point(5,8)
    );

    System.out.println("Lista Original");
    points.forEach(System.out::println);

    System.out.println("Lista Procesada");
    points.stream()
        .mapToLong(OperationsHelper::sumCoords)
        .forEach(System.out::println);

    System.out.println("Valor reducido");
    System.out.println(points.stream()
        .mapToLong(OperationsHelper::sumCoords)
        .sum()
    );
  }

  @Test
  public void sumBiFunction() {
    BiFunction<Integer, Integer, String> sumInt = (x, y) -> String.format("%d + %d = %d", x, y, x+y);

    System.out.println(sumInt.apply(150, 235));
    assertNotNull( sumInt.apply(150, 835) );
  }

  @Test
  public void searchStringsBiFunction() {
    BiFunction<String, List<String>, List<String>> search = (key, list) ->
      list.stream().filter(s -> s.toUpperCase().contains(key.toUpperCase())).collect(Collectors.toList());

    List<String> list = Arrays.asList("NOne", "noNE", "noone",
        "Noguera", "Camino", "dsadNOdasdas", "bonosol", "abc", "15616", "dasdsa486435");

    System.out.println(search.apply("no", list));
    System.out.println(search.apply("bc", list));
    System.out.println(search.apply("0_", list));
  }


  @Test
  public void fullTextSearchPerson() {
    // https://bitbucket.org/sb-ccat/java8-course

    // https://bitbucket.org/sb-ccat/java8-course.git
    BiFunction<String, List<Person>, List<Person>> search = (key, list) ->
        list.stream()
            .filter(s ->
                String.join(s.getName(),String.valueOf(s.getCi()),String.valueOf(s.getAge()))
                    .toUpperCase().contains(key.toUpperCase()))
            .collect(Collectors.toList());

    List<Person> list = Arrays.asList(new Person("Roberto", 1234667, 80),
        new Person("Ana", 55555, 20),
        new Person("Jose", 0, 35));


    System.out.println(search.apply("55", list)); // Ana
    System.out.println(search.apply("0", list)); // Ana
    System.out.println(search.apply("an", list)); // An
  }

  @Test
  public void concatenacionDeFunciones() {
    Function<String, Integer> getLength = s -> s.length();

    //Predicate<Integer, boolean> esPar = s -> s%2 == 0;
    Function<Integer, Boolean> esPar = s -> s%2 == 0;

    Function<Boolean, String> alert = b -> b ? "Longitud par" : "Longitud impar";

    BiFunction<Integer, Integer, Boolean> iguales = (x, y) -> x == y;

    System.out.println(iguales.andThen(alert).apply(5,8));

    System.out.println(getLength.andThen(esPar).andThen(alert).apply("Jesus"));
    System.out.println(getLength.andThen(esPar).andThen(alert).apply("JOSE"));
  }
}