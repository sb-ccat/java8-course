package edu.ccat.sesion1;

public class Student implements Comparable<Student> {

  private String name;
  private int age;
  private long ci;

  public Student() { // Default Constructor
    this.name = "Anonimous";
    this.age = 18;
    this.ci = 12345;
  }

  public Student(String name, int age, long ci) { // Full Constructor
    this.name = name;
    this.age = age;
    this.ci = ci;
  }

  public Student(String name) { // Partial constructor
    this(name, 18, 12345);
  }

  @Override
  public String toString() {
    return String.format("Name: %s, Age: %d, CI: %d", this.name, this.age, this.ci);
  }

  @Override
  public int compareTo(Student other) { // -1 0 +1
    return this.name.compareTo(other.name);
    //Integer.compare(this.age, other.age);
  }

  public int getAge() {
    return this.age;
  }

  public long getCi() {
    return this.ci;
  }
}
