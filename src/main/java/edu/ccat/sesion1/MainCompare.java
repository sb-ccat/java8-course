package edu.ccat.sesion1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainCompare {

  public static void main(String[] args) {

    // Ordenar nombres
    /*
    List<String> names = new ArrayList<>(0);
    names.add("Roberto");
    names.add("Paolo");
    names.add("Juan");

    System.out.println(names);
    Collections.sort(names);
    System.out.println(names);
    */

    // Ordenar una lista de estudiantes
    List<Student> students = new ArrayList<>(0);
    students.add(new Student());
    students.add(new Student("Roberto", 50, 1234678));
    students.add(new Student("Paolo", 80, 986432));
    students.add(new Student("Juan"));

    System.out.println(students);
    Collections.sort(students);
    System.out.println(students);

    Collections.sort(students, new AgeComparator());
    System.out.println(students);

    // Java 7
    Collections.sort(students, new Comparator<Student>() {
      @Override
      public int compare(Student first, Student second) {
        return Integer.compare(first.getAge(), second.getAge());
      }
    });

    // Java 8
    Collections.sort(students,
        (first, second) ->
            Integer.compare(first.getAge(), second.getAge()) );

    Collections.sort(students,
        (first, second) ->
            Long.compare(first.getCi(), second.getCi()));
    System.out.println(students);
  }
}
