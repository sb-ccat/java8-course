package edu.ccat.sesion1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main1 {

  public static void main(String[] args) {
    Object obj = new Object();
    System.out.println(obj);

    Student student = new Student();
    System.out.println(student);

    int[] numbers = {5, 8, 7, 9};
    System.out.println(numbers);

    // Java <= 6
    System.out.println("Java <= 6");
    for (int i = 0; i < numbers.length; i++) {
      System.out.print(numbers[i] + " ");
    }

    // Java >= 7
    System.out.println("Java >= 7");
    for (int aux: numbers) {
      System.out.print(aux + " ");
    }

    // Java 8 Estructuras dinamicas
    System.out.println("Java 8");
    List<Integer> dinamicNumbers = new ArrayList<>(0); //Collections.emptyList();
    dinamicNumbers.add(15);
    dinamicNumbers.add(85);
    //dinamicNumbers.forEach(e -> System.out.println(e));
    dinamicNumbers.forEach(System.out::print);
  }
}
