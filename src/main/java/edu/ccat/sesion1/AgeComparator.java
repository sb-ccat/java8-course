package edu.ccat.sesion1;

import java.util.Comparator;

public class AgeComparator implements Comparator<Student> {
  @Override
  public int compare(Student first, Student second) {
    return Integer.compare(first.getAge(), second.getAge());
  }
}
