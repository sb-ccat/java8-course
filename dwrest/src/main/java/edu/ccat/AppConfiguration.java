package edu.ccat;

import edu.ccat.core.UserConfiguration;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class AppConfiguration extends Configuration {
    // TODO: implement service configuration

  //hibernate-validator
  @NotNull
  @JsonProperty
  private String version;

  @JsonProperty()
  private String saguapac;

  @JsonProperty
  @Valid
  private UserConfiguration uconfig;

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getSaguapac() {
    return saguapac;
  }

  public void setSaguapac(String saguapac) {
    this.saguapac = saguapac;
  }

  public UserConfiguration getUconfig() {
    return uconfig;
  }

  public void setUconfig(UserConfiguration uconfig) {
    this.uconfig = uconfig;
  }
}
