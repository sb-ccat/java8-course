package edu.ccat;

import edu.ccat.core.HibernateModule;
import edu.ccat.core.MainModule;
import edu.ccat.resources.ExampleResource;
import edu.ccat.resources.ExampleService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import ru.vyarus.dropwizard.guice.GuiceBundle;

public class AppApplication extends Application<AppConfiguration> {

    public static void main(final String[] args) throws Exception {
        new AppApplication().run(args);
    }

    @Override
    public String getName() {
        return "App";
    }

    @Override
    public void initialize(final Bootstrap<AppConfiguration> bootstrap) {
        bootstrap.addBundle(GuiceBundle.builder()
            .enableAutoConfig(getClass().getPackage().getName())
            .modules(new MainModule())
            .build());
    }

    @Override
    public void run(final AppConfiguration configuration,
                    final Environment environment) {
        System.out.println("=========================");
        System.out.println(configuration.getSaguapac());
        System.out.println(configuration.getVersion());
        System.out.println(configuration.getUconfig());
    }
}
