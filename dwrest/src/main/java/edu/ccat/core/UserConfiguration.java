package edu.ccat.core;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;

public class UserConfiguration {

  @JsonProperty
  private String credential = "none";

  @JsonProperty
  @Min(5)
  private int experience;

  public UserConfiguration() {
  }

  public String getCredential() {
    return credential;
  }

  public void setCredential(String credential) {
    this.credential = credential;
  }

  public int getExperience() {
    return experience;
  }

  public void setExperience(int experience) {
    this.experience = experience;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("UserConfiguration{");
    sb.append("credential='").append(credential).append('\'');
    sb.append(", experience=").append(experience);
    sb.append('}');
    return sb.toString();
  }
}
