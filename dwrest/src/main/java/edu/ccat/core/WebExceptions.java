package edu.ccat.core;

import org.eclipse.jetty.http.HttpStatus;

import javax.ws.rs.WebApplicationException;

public class WebExceptions {

  public static WebApplicationException notFound() {
    return new WebApplicationException("Resource not found", HttpStatus.NOT_FOUND_404);
  }
}
