package edu.ccat.core;

import com.google.inject.Provides;
import edu.ccat.AppConfiguration;
import edu.ccat.resources.ExampleService;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class MainModule extends DropwizardAwareModule<AppConfiguration> {
  @Override
  protected void configure() {
    bind(ExampleService.class).toInstance(new ExampleService(configuration().getVersion()));
  }

  @Provides
  public ExampleService exampleService() {
    return new ExampleService();
  }
}
