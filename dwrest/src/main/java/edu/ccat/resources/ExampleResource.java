package edu.ccat.resources;

import com.google.inject.Inject;
import com.sun.org.apache.regexp.internal.RE;
import edu.ccat.api.Student;
import edu.ccat.core.WebExceptions;
import org.eclipse.jetty.http.HttpStatus;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;

@Path("/example")
@Produces(MediaType.APPLICATION_JSON)
public class ExampleResource {

  final private ExampleService service;

  @Inject
  public ExampleResource(ExampleService service) {
    this.service = service;
  }


  @GET
  public String sayHello() {
    return "Hello Dropwizard";
  }

  @GET
  @Path("student") // http://localhost:8080/example/student
  public Student getStudent() {
    return new Student(5,"Martin", 65);
  }

  @POST
  @Path("student")
  public Response setStudent(@QueryParam("name") String name,
                             @QueryParam("age") int age) {
    //new Student(name, age);
    return Response.created(URI.create("students/5")).build();
  }

  @GET
  @Path("student/{id}")
  public Response getStudentById(@PathParam("id") long id) {
    return Response.ok(this.service.getStudentById(id)
        .orElseThrow(WebExceptions::notFound))
      .build();
  }

  @GET
  @Path("version")
  public String getSystemVersion() {
    return this.service.getVersion();
  }
}
