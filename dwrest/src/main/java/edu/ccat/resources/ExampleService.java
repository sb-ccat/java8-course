package edu.ccat.resources;

import edu.ccat.api.Student;
import edu.ccat.db.StudentDao;

import java.util.Optional;

public class ExampleService {

  final private String version;

  public ExampleService(String version) {
    this.version = version;
  }

  // Asumimos que en la base de datos solo hay Student con id < 5
  protected Optional<Student> getStudentById(long id) {

    return Optional.ofNullable(
        (id <= 5)
            ? new Student(id, "Xavier", 85)
            : null);
  }

  protected String getVersion() {
    return this.version;
  }
}
