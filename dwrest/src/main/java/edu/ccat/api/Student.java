package edu.ccat.api;

import com.fasterxml.jackson.annotation.JsonProperty;

// POJO: Plain Older Java Object
public class Student {

  @JsonProperty
  private long id;

  @JsonProperty
  private String name;

  @JsonProperty
  private int age;

  public Student() { // Para facilitar la serializacion/deserealizacion con Jackson
  }

  public Student(long id, String name, int age) {
    this.id = id;
    this.name = name;
    this.age = age;
  }

  public String getName() {
    return this.name;
  }

  public int getAge() {
    return this.age;
  }

  public long getId() {
    return this.id;
  }
}
