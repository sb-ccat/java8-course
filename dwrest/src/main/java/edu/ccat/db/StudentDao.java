package edu.ccat.db;

import edu.ccat.api.Student;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Optional;

public class StudentDao extends AbstractDAO<Student> {

  public StudentDao(SessionFactory sessionFactory) {
    super(sessionFactory);
  }

  public Optional<Student> getById(long id) {
    return  Optional.ofNullable(get(id));
  }
}
